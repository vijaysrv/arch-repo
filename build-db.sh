#!/usr/bin/env bash
#
# Script name: build-db.sh
# Description: Script for rebuilding the database for vijay-repo.
# GitLab: https://www.gitlab.com/dwt1/vijay-repo
# Contributors: Derek Taylor

# Set with the flags "-e", "-u","-o pipefail" cause the script to fail
# if certain things happen, which is a good thing.  Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

x86_pkgbuild=$(find ../vijay-pkgbuild/x86_64 -type f -name "*.pkg.tar.zst*")

for x in ${x86_pkgbuild}
do
    mv "${x}" x86_64/
    echo "Moving ${x}"
done

echo "###########################"
echo "Building the repo database."
echo "###########################"

## Arch: x86_64
cd x86_64
rm -f vijay-repo*

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

## repo-add
## -s: signs the packages
## -n: only add new packages not already in database
## -R: remove old package files when updating their entry
repo-add -s -n -R vijay-repo.db.tar.gz *.pkg.tar.zst

# Removing the symlinks because GitLab can't handle them.
rm vijay-repo.db
rm vijay-repo.db.sig
rm vijay-repo.files
rm vijay-repo.files.sig

# Renaming the tar.gz files without the extension.
mv vijay-repo.db.tar.gz vijay-repo.db
mv vijay-repo.db.tar.gz.sig vijay-repo-db.sig
mv vijay-repo.files.tar.gz vijay-repo.files
mv vijay-repo.files.tar.gz.sig vijay-repo.files.sig

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"

git add .
git commit -m "Updated the repo database. $("date")"
git push

find ~/git/Arch-Pkg-Repo/vijay-pkgbuild/x86_64  -type f -not -name PKGBUILD -not -name "*.install" -delete
